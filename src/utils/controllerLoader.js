const path = require('path');
const fs = require('fs');

const _defaultPathPrefix = 'controller'
const _defaultControllerSuffix = 'Controller'

let appPath = path.normalize(path.join(__dirname, '../..'))

console.log("[utils/controllerLoader] appPath = ", appPath)

class ControllerLoader {

  constructor() {
    this.pathPrefix = arguments[0] || _defaultPathPrefix
    this.controllerSurffix = arguments[1] || _defaultControllerSuffix
  }

  targetToPath(target) {
    // replace . with /
    // eg. accounts.foo => accounts/foo
    return target.replace(/\./gi, '/')
  }

  buildControllerRelativePath(target) {
    return path.join(
      this.pathPrefix,
      target + this.controllerSurffix
    )
  }

  fullPathFromTarget(target) {
    let targetPath = this.targetToPath(target)
    let controllerPath = this.buildControllerRelativePath(targetPath)
    let fullPath = path.join(appPath, controllerPath + '.js')

    return fullPath
  }

  loadController(target) {
    let fullPath = this.fullPathFromTarget(target)
    if (!fs.existsSync(fullPath)) {
      console.log(`[ControllerLoader.loadController] Error: controller (${target}) not found at ${fullPath}`)
      return null;
    }

    let controllerModule = null
    try {
      controllerModule = require(fullPath)
    } catch (err) {
      console.log('[ControllerLoader#loadController] Error: ', err)
    }

    return controllerModule
  }
}

module.exports = {
  ControllerLoader
}
