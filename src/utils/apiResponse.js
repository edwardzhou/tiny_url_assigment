/*
 * Exception class
 */
const corsHeaders = {
  'Access-Control-Allow-Origin': process.env.CORS_ALLOW_ORIGIN || 'www.checkyourconfig.com',
  'Access-Control-Allow-Methods': process.env.CORS_ALLOW_METHODS || '',
  'Access-Control-Allow-Headers': process.env.CORS_ALLOW_HEADERS || ''
}

/**
 * 580:将第三方接口返回的错误信息返回到我们的前端
 * 581:我们自己的接口的错误信息返回到我们的前端
 */
class ApiResponse {

  static get E_NOT_IMPLEMENTED () {
    return '501'
  }

  static success (res = {}, statusCode = 200, headers = {}) {
    let respBody
    if (res == null) {
      respBody = ''
    } else if (typeof res == 'string') {
      respBody = res
    } else {
      respBody = JSON.stringify(res)
    }

    let respHeaders = Object.assign({}, headers)
    Object.assign(respHeaders, corsHeaders)

    const response = {
      statusCode: statusCode,
      headers: respHeaders,
      body: respBody
    }
    return response
  }

  static errorMessage (error, statusCode = 500) {
    let errorMsg = typeof error == 'string' ? error : JSON.stringify(error)

    const response = {
      statusCode: statusCode,
      headers: corsHeaders,
      body: errorMsg
    }
    
    return response
  }

  static error (statusCode = 500, customCode = ApiResponse.E500, error, data = {}) {
    const response = {
      statusCode: statusCode,
      headers: corsHeaders,
      body: JSON.stringify({
        code: customCode,
        msg: error.toString(),
        data
      })
    }
    return response
  }
}

module.exports = {
  ApiResponse
}
