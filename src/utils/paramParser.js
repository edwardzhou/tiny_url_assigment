const apiGatewayHttpParamParser = require('./apiGatewayHttpParamParser')
// restful-router http/sqs请求 派发并返回参数
class ParamParser {
  parse (event) {
    return apiGatewayHttpParamParser.parse(event)
  }
}

module.exports = new ParamParser()
