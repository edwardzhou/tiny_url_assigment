const crypto = require('crypto')
const base36 = require('base36')

class UrlDigest {

  static digest(url) {
    let md5 = crypto.createHash('md5')
    md5.update(url)
    let digest = md5.digest('hex')
    
    let scode = base36.base36encode(
      Date.now() * 1000 + Math.floor(Math.random() * 1000)
    )

    return {
      scode: scode,
      md5: digest,
      rawUrl: url
    }
  }
}

module.exports = { UrlDigest }
