// restful-router http请求 参数处理
class ApiGatewayHttpParamParser {
  static parse (event) {
    // Avoid altering original object by Object.assign 
    let params = Object.assign({}, event.queryStringParameters || {})
    if (event.body) {
      // Merge POST data
      Object.assign(params, JSON.parse(event.body))
    }

    const result = {
      target: (!!event.pathParameters && event.pathParameters.target) || null,
      proxy: (!!event.pathParameters && event.pathParameters.proxy) || null,
      params,
      httpMethod: event.httpMethod
    }

    return result
  }
}

module.exports = ApiGatewayHttpParamParser
