const { ApiResponse } = require('./apiResponse')
const { ControllerLoader } = require('./controllerLoader')
const paramParser = require('./paramParser')

const defaultOptionsMethod = async(_p1, _p2, callback) => {
  return callback(null, ApiResponse.success({}))
}

const parseRestfulMethod = (httpMethod, resourceId) => {
  let restfulMethod = null

  if (httpMethod === 'POST' && resourceId == null) {
    restfulMethod = 'create'
  } else if (httpMethod === 'GET' && resourceId == null) {
    restfulMethod = 'list'
  } else if (httpMethod === 'GET' && !!resourceId) {
    restfulMethod = 'show'
  } else if (httpMethod === 'PUT' && !!resourceId) {
    restfulMethod = 'update'
  } else if (httpMethod === 'DELETE' && !!resourceId) {
    restfulMethod = 'destroy'
  } else if (httpMethod === 'OPTIONS') {
    restfulMethod = 'options'
  }

  return restfulMethod
}

/**
 * 
 * @param {*} param0 
 */
const dispatcher = async ({ event, context, callback, controllerClass = null, pathPrefix = null, controllerSuffix = null }) => {
  context.callbackWaitsForEmptyEventLoop = false

  const { params, proxy, target, httpMethod } = paramParser.parse(event, context)

  // console.log('[restful-router.dispatcher] event.httpMethod: ' + httpMethod)
  // console.log('[restful-router.dispatcher] params: ' + JSON.stringify(params))
  // console.log('[restful-router.dispatcher] event.pathParameters.proxy: ' + proxy)
  // console.log('[restful-router.dispatcher] event.pathParameters.target: ' + target)
  // console.log('[restful-router.dispatcher] pathPrefix: ' + pathPrefix)
  // console.log('[restful-router.dispatcher] controllerSuffix: ' + controllerSuffix)

  let controller = null

  if (target) {
    let loader = new ControllerLoader(pathPrefix, controllerSuffix)
    let controllerModule = loader.loadController(target)
    if (controllerModule == null) {
      let message = `Error: unable to load controller for '${target}' with [${pathPrefix}, ${controllerSuffix}]`
      return callback(null, ApiResponse.error(500, ApiResponse.E5001, message))
    }
    controllerClass = controllerModule.clazz
  }

  if (typeof controllerClass === 'object') {
    controller = controllerClass
  } else if (typeof controllerClass === 'function') {
    /* eslint-disable new-cap */
    controller = new controllerClass()
  } else {
    return callback(null, ApiResponse.error(500, ApiResponse.E5001, 'ERROR: unknown controllerClass type [' + typeof controllerClass + ']'))
  }

  const resourceId = proxy
  params.paramId = null
  if (resourceId) {
    params.paramId = resourceId
  }

  let restfulMethod = parseRestfulMethod(httpMethod, resourceId)
  let controllerMethod = controller[restfulMethod]
  if (restfulMethod === 'options') {
    controllerMethod = controllerMethod || defaultOptionsMethod
  }
  
  if (!restfulMethod || !controllerMethod) {
    console.error('[router] unable to find matched controller method `' + restfulMethod + '` for ' + httpMethod + ' ' + resourceId)
    return callback(null, ApiResponse.error(501, ApiResponse.E_NOT_IMPLEMENTED, `cannot find method '${restfulMethod}'`))
  }

  const apiGateway = {
    event: event,
    context: context,
    callback: callback
  }
  
  try {
    return await controllerMethod.apply(controller, [apiGateway, params, callback])
  } catch (err) {
    console.error('ERROR: [router] exception caught: ', err)
    return callback(null, ApiResponse.error(500, "5001", JSON.stringify(err)))
  }
}

/**
 * router for restful protocol.
 * HTTP VER   Resources          handler method
 * GET        /resources    ->   controllerClass#list
 * POST       /resources    ->   controllerClass#create
 * GET        /resources/id ->   controllerClass#show
 * PUT        /resources/id ->   controllerClass#update
 * DELETE     /resources/id ->   controllerClass#destroy
 * OPTION     /resources    ->   controllerClass#option (optional)
 *
 * controllerClass can only implement supported actions. eg. only #list and #create
 * those unimplemented actions will get HTTP 501.
 *
 * @param {*} controllerClass - controller class to wrap with Restful style
 */
const router = function (controllerClass) {
  return async function (event, context, callback) {
    await dispatcher({ event, context, callback, controllerClass })
  }
}

const autoRouter = function (pathPrefix = null, controllerSuffix = null) {
  return async function (event, context, callback) {
    await dispatcher({ event, context, callback, pathPrefix, controllerSuffix })
  }
}

module.exports = {
  Router: router,
  AutoRouter: autoRouter,
  autoHandler: autoRouter(process.env['PATH_PREFIX'], process.env['CONTROLLER_SURFFIX'])
}
