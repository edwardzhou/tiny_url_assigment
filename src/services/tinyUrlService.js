const AWS = require("aws-sdk")
const { UrlDigest } = require("../utils/urlDigest")

const tableName = 'tinyUrls'

class TinyUrlService {

  constructor() {
    this.docClient = new AWS.DynamoDB.DocumentClient()
  }

  async urlExists(url) {
    let data = await this.getByUrl(url)
    return data.Count > 0
  }

  async shorten(url) {
    let digested = UrlDigest.digest(url)
    let item = {
      scode: digested.scode,
      rawUrl: url,
      md5: digested.md5
    }

    let params = {
      TableName: tableName,
      Item: item
    }

    await this.docClient.put(params).promise()
    return item
  }

  async get(scode) {
    let data = await this.getByScode(scode)

    let result = null
    if (data.Count > 0) {
      return data.Items[0]
    }

    return result

  }

  async getByUrl(url) {
    let query = {
      TableName: tableName,
      FilterExpression: 'md5 = :md5',
      ExpressionAttributeValues: {
        ':md5': UrlDigest.digest(url).md5
      }
    }

    // console.log('[TinyUrlService.urlExists] query => ', query)

    let data = await this.docClient.scan(query).promise()

    // console.log('[TinyUrlService.urlExists] data => ', data)

    return data
  }

  async getByScode(scode) {
    let query = {
      TableName: tableName,
      FilterExpression: 'scode = :scode',
      ExpressionAttributeValues: {
        ':scode': scode
      }
    }

    console.log('[TinyUrlService.getByScode] query => ', query)

    return await this.docClient.scan(query).promise()

    // console.log('[TinyUrlService.urlExists] data => ', data)
  }
}

module.exports = { TinyUrlService }
