exports.handler = function(event, context, callback) {
  context.callbackWaitsForEmptyEventLoop = false

  return callback(null, {
    statusCode: 200,
    headers: {},
    body: 'Hello AWS Lambda~ with CI/CD'
  })
}
