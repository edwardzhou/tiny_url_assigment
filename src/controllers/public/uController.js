const { ApiResponse } = require('../../utils/apiResponse')
const { TinyUrlService } = require('../../services/tinyUrlService')

class UrlController {
  async show(apiGateway, params, callback) {
    let service = new TinyUrlService()
    let item = await service.get(params.paramId)

    if (item == null) {
      return callback(null, ApiResponse.errorMessage(`${params.paramId} is not found`, 404))
    }

    let headers = {
      "Location": item.rawUrl
    }
    return callback(null, ApiResponse.success("", 301, headers))
  }
}

module.exports = {
  clazz: UrlController,
  UrlController
}
