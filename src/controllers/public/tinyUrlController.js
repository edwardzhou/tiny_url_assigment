const { ApiResponse } = require("../../utils/apiResponse")
const { TinyUrlService } = require("../../services/tinyUrlService")

class TinyUrlController {
  constructor() {
    this.service = new TinyUrlService()
  }

  async create(apiGateway, params, callback) {
    let url = params.url

    let appKey = apiGateway.event.headers['x-app-key'] || "unknown"
    if (appKey != process.env.X_APP_KEY) {
      return callback(null, ApiResponse.errorMessage(`Authentication denied.`, 403))
    }

    if (url == null) {
      return callback(null, ApiResponse.errorMessage(`url required`, 500))
    }

    if (await this.service.urlExists(url)) {
      return callback(null, ApiResponse.errorMessage(`${url} already exists`, 500))
    }

    let result = await this.service.shorten(params.url)
    this.fillFullUrls(apiGateway, result)

    return callback(null, ApiResponse.success(result, 200))
  }

  fillFullUrls(apiGateway, item) {
    let host = apiGateway.event.headers.Host
    let custom_domain = process.env["CUSTOM_DOMAIN"] || `${host}/latest/g/u`
    item.tinyUrl = `https://${custom_domain}/${item.scode}`
    item.awsUrl = `https://${host}/latest/g/u/${item.scode}`
  } 
}

module.exports = {
  clazz: TinyUrlController, // for auto-router
  TinyUrlController
}
