//// restful-router http请求/sqs请求 参数 单元测试用例
const chai = require('chai')
const spies = require('chai-spies')
const expect = chai.expect

const paramParser = require('../../src/utils/paramParser')
chai.use(spies)

describe('ApiGatewayHttpParamParser',async()=>{
  let context ={}
  describe('GET /resources',async ()=>{
    let event = {}
    
    it("success with httpParams", async() => {
      event = {
        'queryStringParameters': {
        },
        'pathParameters': {
          'target': 'controllerLoader.dummy'
        },  
        'queryStringParameters':{foo:'bar'},
        'headers': {
        },
        'httpMethod': 'GET',
      }

      result = paramParser.parse(event, context)
  
      expect(result.target).to.eq('controllerLoader.dummy')
      expect(result.proxy).to.eq(null)
      expect(result.params).to.deep.eq({foo:'bar'})
      expect(result.httpMethod).to.eq('GET')
    }) 
  })
})

