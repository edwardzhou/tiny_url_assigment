/* global describe, before, it */
const chai = require('chai')
const spies = require('chai-spies')
const expect = chai.expect

// const {DummyService} = require('../../service/dummyService')
const {ControllerLoader} = require('../../src/utils/controllerLoader')

chai.use(spies)

const sandbox = chai.spy.sandbox();

describe('ControllerLoader', async () => {

  describe('#targetToPath', async() => {
    let loader = new ControllerLoader()

    it('should returns foo with "foo"', async() => {
      expect(loader.targetToPath('foo'))
        .to.eql('foo')
    })

    it('should returns accounts/foo with "accounts.foo"', async() => {
      expect(loader.targetToPath('accounts.foo'))
        .to.eql('accounts/foo')
    })

    it('should returns accounts/users/foo with "accounts.users.foo"', async() => {
      expect(loader.targetToPath('accounts.users.foo'))
        .to.eql('accounts/users/foo')
    })

    it('should returns fooBar with "fooBar"', async() => {
      expect(loader.targetToPath('fooBar'))
        .to.eql('fooBar')
    })

    it('should returns accounts/fooBar with "accounts.fooBar"', async() =>{
      expect(loader.targetToPath('accounts.fooBar'))
        .to.eql('accounts/fooBar')
    })
  })

  describe('#buildControllerRelativePath', async() => {
    let loader = new ControllerLoader()

    it('should returns controller/fooController with "foo"', async() => {
      expect(loader.buildControllerRelativePath('foo')).to.eql('controller/fooController')
    })

    it('should returns controller/subfolder/fooController with "subfolder/foo"', async() =>{
      expect(loader.buildControllerRelativePath('subfolder/foo'))
        .to.eql('controller/subfolder/fooController')
    })
  })

  describe('#loadController', async() => {
    let loader = new ControllerLoader('test/controllers')

    it('should returns controller class', async() => {
      let controllerModule = loader.loadController('controllerLoader.dummy')
      let controllerClass = controllerModule.clazz
      expect(controllerClass).not.to.eql(null)
    })

    it('should returns null for non-exist controller', async() => {
      let controllerModule = loader.loadController('controllerLoader.dummyNonExist')
      expect(controllerModule).to.eql(null)
    })
  })
})
