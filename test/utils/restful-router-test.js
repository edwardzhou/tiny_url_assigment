/* global describe, before, it */
const {Router, AutoRouter} = require('../../src/utils/restful-router')
const chai = require('chai')
const spies = require('chai-spies')
const expect = chai.expect

chai.use(spies)

const sandbox = chai.spy.sandbox();

class DummyController {
  constructor() {

  }
}

describe('router', function() {
  let router
  let dummy_controller
  let event
  let context = {}

  beforeEach(function(){
    dummy_controller = new DummyController();
    router = Router(dummy_controller);
  });

  afterEach(function(){
    sandbox.restore();
  })

  describe('GET /resources', function() {

    beforeEach(function(){
      event = {
        'queryStringParameters': {
        },
        'headers': {
        },
        'httpMethod': 'GET',
      }

      sandbox.on(dummy_controller, ['list'])
    })

    it('controller#list should be called', async function(){
      let callback = function(err, resp) {};

      await router(event, context, callback);

      expect(dummy_controller.list).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {paramId: null},
        callback)
    })

    it('controller#list should be called with query parameters', async function(){
      let callback = function(err, resp) {};

      event.queryStringParameters = {'foo': 'bar'};

      await router(event, context, callback);

      expect(dummy_controller.list).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {foo: 'bar', paramId: null},
        callback)
    })

    it('should return error 501 when controller has no #list', async function() {
      let result

      sandbox.restore()

      await router(event, context, function(err, resp) {
        result = resp
      })

      expect(result.statusCode).to.eq(501);
    });

    it('controller#list should not be called if with {proxy}', async function() {
      let callback = function(err, resp) {};
      event.pathParameters = {
        'proxy': 'abc'
      }

      await router(event, context, callback);
      expect(dummy_controller.list).not.to.have.been.called();
    })
  });

  describe('GET /resources/{proxy}', function(){
    let event
    let context = {}

    beforeEach(function(){
      event = {
        'queryStringParameters': {
        },
        'headers': {
        },
        'pathParameters': {
          'proxy': 'abc123'
        },
        'httpMethod': 'GET',
      }

      sandbox.on(dummy_controller, ['show'])

    });

    it('controller#show should be called with paramId', async function(){
      let callback = function(err, resp) {};

      await router(event, context, callback);

      expect(dummy_controller.show).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {paramId: 'abc123'},
        callback
      );
    });

    it('controller#show should be called with query parameters', async function(){
      let callback = function(err, resp) {};

      event.queryStringParameters = {'foo': 'bar'};

      await router(event, context, callback);

      expect(dummy_controller.show).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {foo: 'bar', paramId: 'abc123'},
        callback)
      ;
    });

    it('should return error 501 when controller has no #show', async function() {
      let result

      sandbox.restore()

      await router(event, context, function(err, resp) {
        result = resp
      })

      expect(result.statusCode).to.eq(501);
    });
  });

  describe('POST /resources', function(){
    let event
    let context = {}

    beforeEach(function(){
      form_body = {
        field_1: 'value'
      }

      event = {
        'body': JSON.stringify(form_body),
        'queryStringParameters': {
        },
        'headers': {
        },
        'httpMethod': 'POST',
      }

      sandbox.on(dummy_controller, ['create'])
    });

    it('controller#create should be called', async function(){
      let callback = function(err, resp) {};

      await router(event, context, callback);

      expect(dummy_controller.create).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {field_1: 'value', paramId: null},
        callback
      );
    });

    it('controller#create should be called with query parameters', async function(){
      let callback = function(err, resp) {};

      event.queryStringParameters = {'foo': 'bar'};

      await router(event, context, callback);

      expect(dummy_controller.create).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {field_1: 'value', foo: 'bar', paramId: null},
        callback)
      ;
    });

    it('should return error 501 when controller has no #create', async function() {
      let result

      sandbox.restore()

      await router(event, context, function(err, resp) {
        result = resp
      })

      expect(result.statusCode).to.eq(501);
    });
  });

  describe('PUT /resources/{proxy}', function(){
    beforeEach(function(){
      form_body = {
        field_1: 'value'
      }

      event = {
        'body': JSON.stringify(form_body),
        'queryStringParameters': {
        },
        'headers': {
        },
        'pathParameters': {
          'proxy': 'abc123'
        },
        'httpMethod': 'PUT',
      }

      sandbox.on(dummy_controller, ['update'])
    });

    it('controller#update should be called', async function(){
      let callback = function(err, resp) {};

      await router(event, context, callback);

      expect(dummy_controller.update).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {field_1: 'value', paramId: 'abc123'},
        callback
      );
    });

    it('controller#update should be called with query parameters', async function(){
      let callback = function(err, resp) {};

      event.queryStringParameters = {'field_1': 'abc', 'foo': 'bar'};

      await router(event, context, callback);

      expect(dummy_controller.update).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {field_1: 'value', foo: 'bar', paramId: 'abc123'},
        callback)
      ;
    });

    it('should return error 501 when controller has no #update', async function() {
      let result

      sandbox.restore()

      await router(event, context, function(err, resp) {
        result = resp
      })

      expect(result.statusCode).to.eq(501);
    });
  });

  describe('DELETE /resources/{proxy}', function(){
    beforeEach(function(){
      form_body = {
        field_1: 'value'
      }

      event = {
        'body': JSON.stringify(form_body),
        'queryStringParameters': {
        },
        'headers': {
        },
        'pathParameters': {
          'proxy': 'abc123'
        },
        'httpMethod': 'DELETE',
      }

      sandbox.on(dummy_controller, ['destroy'])
    });

    it('controller#destroy should be called', async function(){
      let callback = function(err, resp) {};

      await router(event, context, callback);

      expect(dummy_controller.destroy).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {field_1: 'value', paramId: 'abc123'},
        callback
      );
    });

    it('controller#destroy should be called with query parameters', async function(){
      let callback = function(err, resp) {};

      event.queryStringParameters = {'foo': 'bar'};

      await router(event, context, callback);

      expect(dummy_controller.destroy).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {field_1: 'value', foo: 'bar', paramId: 'abc123'},
        callback)
      ;
    });

    it('should return error 501 when controller has no #destroy', async function() {
      let result

      sandbox.restore()

      await router(event, context, function(err, resp) {
        result = resp
      })

      expect(result.statusCode).to.eq(501);
    });
  });

  describe('OPTIONS /resources', function(){
    beforeEach(function(){
      form_body = {
        field_1: 'value'
      }

      event = {
        'body': JSON.stringify(form_body),
        'queryStringParameters': {
        },
        'headers': {
        },
        'pathParameters': {
        },
        'httpMethod': 'OPTIONS',
      }

      sandbox.on(dummy_controller, ['options'])
    });

    it('controller#options should be called', async function(){
      let callback = function(err, resp) {};

      await router(event, context, callback);

      expect(dummy_controller.options).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {field_1: 'value', paramId: null},
        callback
      );
    });

    it('controller#options should be called with query parameters', async function(){
      let callback = function(err, resp) {};

      event.queryStringParameters = {'foo': 'bar'};

      await router(event, context, callback);

      expect(dummy_controller.options).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {field_1: 'value', foo: 'bar', paramId: null},
        callback)
      ;
    });

    it('should return error 200 when controller has no #options', async function() {
      let result

      sandbox.restore()

      await router(event, context, function(err, resp) {
        result = resp
      })

      expect(result.statusCode).to.eq(200);
    });
  });

  describe('OPTIONS /resources/{proxy}', function(){
    beforeEach(function(){
      form_body = {
        field_1: 'value'
      }

      event = {
        'body': JSON.stringify(form_body),
        'queryStringParameters': {
        },
        'headers': {
        },
        'pathParameters': {
          'proxy': 'abc123'
        },
        'httpMethod': 'OPTIONS',
      }

      sandbox.on(dummy_controller, ['options'])
    });

    it('controller#options should be called', async function(){
      let callback = function(err, resp) {};

      await router(event, context, callback);

      expect(dummy_controller.options).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {field_1: 'value', paramId: 'abc123'},
        callback
      );
    });

    it('controller#options should be called with query parameters', async function(){
      let callback = function(err, resp) {};

      event.queryStringParameters = {'foo': 'bar'};

      await router(event, context, callback);

      expect(dummy_controller.options).to.have.been.called.with(
        {event: event, context: context, callback: callback},
        {field_1: 'value', foo: 'bar', paramId: 'abc123'},
        callback)
      ;
    });

    it('should return error 200 when controller has no #options', async function() {
      let result

      sandbox.restore()

      await router(event, context, function(err, resp) {
        result = resp
      })

      expect(result.statusCode).to.eq(200);
    });
  });
  
  describe('auto_router', async() => {
    let router = AutoRouter('test/controllers')
    describe('GET /dispatch/controllerLoader.dummy', async() => {

      beforeEach(function(){
        event = {
          'queryStringParameters': {
          },
          'pathParameters': {
            'target': 'controllerLoader.dummy'
          },
          'headers': {
          },
          'httpMethod': 'GET',
        }
  
        // sandbox.on(dummy_controller, ['list'])
      })
  
      it('controller#list should be called', async function(){  
        let response = null
        let callback = function(err, resp) {
          response = resp
        };

        await router(event, context, callback);

        let json = JSON.parse(response.body)
        expect(json["result"]).to.eql("ok")
      })

      it('controller#list should be called with query parameters', async function(){
        let response = null
        let callback = function(err, resp) {
          response = resp
        };
  
        event.queryStringParameters = {'foo': 'bar'};
  
        await router(event, context, callback);
  
        let json = JSON.parse(response.body)
        expect(json["result"]).to.eql("ok")
        expect(json["params"]["foo"]).to.eql("bar")
        expect(json["params"]["paramId"]).to.eql(null)
      })
    })

    describe("GET /dispatch/controllerLoader.dummy/{proxy}", async() => {
      let event
      let context = {}
  
      beforeEach(function(){
        event = {
          'queryStringParameters': {
          },
          'headers': {
          },
          'pathParameters': {
            'target': 'controllerLoader.dummy',
            'proxy': 'abc123'
          },
          'httpMethod': 'GET',
        }  
      });
  
      it('should return error 501 when dummy has no #show', async function() {
        let result
    
        await router(event, context, function(err, resp) {
          result = resp
        })
  
        expect(result.statusCode).to.eq(501);
      });
    })
  });
});
