const chai = require('chai')
const spies = require('chai-spies')
const expect = chai.expect
const crypto = require('crypto')
const { TinyUrlController } = require('../../../src/controllers/public/tinyUrlController')
const { UrlDigest } = require('../../../src/utils/urlDigest')
const { DataFixture } = require('../../dataFixture')

chai.use(spies);
const sandbox = chai.spy.sandbox();

describe("TinyUrlsController", async() => {
  let testController
  let apiGateway

  beforeEach(async () => {
    testController = new TinyUrlController()
    process.env.X_APP_KEY = "foobar"

    apiGateway = {
      event: {
        headers: {
          'x-app-key': "foobar"
        }
      },
      context: {}
    }

    await DataFixture.createTable()
  })

  afterEach(async () => {
    sandbox.restore()
    await DataFixture.dropTable()
  })

  describe("#create", async () => {
    it("shorten new url", async () => {
      let response = null
      let params = {
        url: "https://www.foo.com/"
      }

      let digested = UrlDigest.digest(params.url)
      await testController.create(apiGateway, params, (err, resp) => {
        response = resp
      })

      expect(response.statusCode).to.eq(200)

      let actual = JSON.parse(response.body)
      let expected = {
        scode: digested.scode,
        rawUrl: "https://www.foo.com/",
        md5: digested.md5
      }

      expect(actual.rawUrl).to.deep.eq(expected.rawUrl)
      expect(actual.md5).to.deep.eq(expected.md5)
    })

    it("should failed when url already exists", async () => {
      let response = null
      let params = {
        url: "https://www.foobar.com/"
      }

      await DataFixture.fixture(params.url)
      await testController.create(apiGateway, params, (err, resp) => {
        response = resp
      })

      expect(response.statusCode).to.eq(500)
      expect(response.body).to.eq(`${params.url} already exists`)
    })

    it("should failed when url is missing", async () => {
      let response = null
      let params = {}
  
      await testController.create(apiGateway, params, (err, resp) => {
        response = resp
      })
  
      expect(response.statusCode).to.eq(500)
      expect(response.body).to.eq(`url required`)
    })

    it("should failed when x-app-key is missing", async () => {
      let response = null
      let params = {}
  
      apiGateway.event.headers = {}

      await testController.create(apiGateway, params, (err, resp) => {
        response = resp
      })
  
      expect(response.statusCode).to.eq(403)
      expect(response.body).to.eq(`Authentication denied.`)
    })

    it("should failed when x-app-key is unmatched", async () => {
      let response = null
      let params = {}
  
      apiGateway.event.headers = { "x-app-key": "non_exists" }

      await testController.create(apiGateway, params, (err, resp) => {
        response = resp
      })
  
      expect(response.statusCode).to.eq(403)
      expect(response.body).to.eq(`Authentication denied.`)
    })
  })
})
