const chai = require('chai')
const spies = require('chai-spies')
const expect = chai.expect

const { UrlController } = require('../../../src/controllers/public/uController')
const { DataFixture } = require('../../dataFixture')

describe("controllers/public/uController", async() => {
  let testController
  let apiGateway = {
    event: {},
    context: {}
  }

  beforeEach(async() => {
    testController = new UrlController()
    await DataFixture.createTable()
  })

  afterEach(async() => {
    await DataFixture.dropTable()
  })

  it("should response 301 for valid scode", async() => {
    let response
    let url = 'https://www.jianshu.com/u/78cd037b5055/'

    let item = await DataFixture.fixture(url)

    await testController.show(apiGateway, {paramId: item.scode}, async(err, resp) => { response = resp })
    expect(response.statusCode).to.eq(301)
    expect(response.headers["Location"]).to.eq(url)
  })

  it("should response 404 for invalid scode", async() => {
    let response
    let url = 'https://www.jianshu.com/u/78cd037b5055/'

    let item = await DataFixture.fixture(url)
    let scode = 'abc_not_found'

    await testController.show(apiGateway, {paramId: scode}, async(err, resp) => { response = resp })
    expect(response.statusCode).to.eq(404)
    expect(response.body).to.eq(`${scode} is not found`)
  })
})