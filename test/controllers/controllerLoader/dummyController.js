const { ApiResponse } = require('../../../src/utils/apiResponse')

class DummyController {
  async list(apiGateway, params, callback) {
    return callback(null, ApiResponse.success({result: "ok", params: params}, 200))
  }
}

module.exports = {
  clazz: DummyController,
  DummyController
}
