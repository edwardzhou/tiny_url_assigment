const AWS = require("aws-sdk")

aws_config = {
  region: "us-west-1",
  endpoint: process.env.DYNAMODB_ENDPOINT || "http://localhost:8000"
}
console.log('[Setup] aws_config => ', aws_config)

if (process.env.AWS_ACCESS_KEY) {
  aws_config.accessKeyId = process.env.AWS_ACCESS_KEY
  aws_config.secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY
}

AWS.config.update(aws_config);
