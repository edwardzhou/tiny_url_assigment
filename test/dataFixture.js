const AWS = require("aws-sdk")
const crypto = require('crypto')

function digestUrl(url) {
  let md5 = crypto.createHash('md5')
  md5.update(url)
  let digest = md5.digest()
  return {
    scode: digest.toString('base64'),
    digest: digest.toString('hex')
  }
}


class DataFixture {
  static async createTable() {
    const dynamodb = new AWS.DynamoDB()
    var tableDef = {
      TableName: 'tinyUrls',
      KeySchema: [
        { AttributeName: "scode", KeyType: "HASH" },
        { AttributeName: "md5", KeyType: "RANGE" }
      ],
      AttributeDefinitions: [
        { AttributeName: "scode", AttributeType: "S" },
        { AttributeName: "md5", AttributeType: "S" }
      ],
      ProvisionedThroughput: {
        ReadCapacityUnits: 5,
        WriteCapacityUnits: 5
      }
    }
  
    await dynamodb.createTable(tableDef).promise()
  }
  
  static async dropTable() {
    const dynamodb = new AWS.DynamoDB()
    var tableDef = {
      TableName: 'tinyUrls'
    }
  
    await dynamodb.deleteTable(tableDef).promise()
  }
  
  static async fixture(url) {
    let digested = digestUrl(url)
  
    let docClient = new AWS.DynamoDB.DocumentClient()
    let itemData = {
      TableName: 'tinyUrls',
      Item: {
        "scode": digested.scode,
        "md5": digested.digest,
        "rawUrl": url
      }
    }
  
    await docClient.put(itemData).promise()
    
    return itemData.Item
  }  
}

module.exports = { DataFixture }
