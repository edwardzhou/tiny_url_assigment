# Tiny URL Assigment

## Introduction

【quoted from Wikipedia [https://en.wikipedia.org/wiki/URL_shortening]】

**URL shortening** is a technique on the World Wide Web in which a Uniform Resource Locator (URL) may be made substantially shorter and still direct to the required page. This is achieved by using a redirect which links to the web page that has a long URL. For example, the URL "https://example.com/assets/category_B/subcategory_C/Foo/" can be shortened to "https://snip.ml/Foo", and the URL "https://example.com/about/index.html" can be shortened to "https://snip.ml/h2iBY". Often the redirect domain name is shorter than the original one. 

There are several reasons to use URL shortening. Often regular unshortened links may be aesthetically unpleasing. Many web developers pass descriptive attributes in the URL to represent data hierarchies, command structures, transaction paths or session information. This can result in URLs that are hundreds of characters long and that contain complex character patterns. Such URLs are difficult to memorize, type-out or distribute. As a result, long URLs must be copied-and-pasted for reliability. Thus, short URLs may be more convenient for websites or hard copy publications (e.g. a printed magazine or a book), the latter often requiring that very long strings be broken into multiple lines (as is the case with some e-mail software or internet forums) or truncated.

## Service design

![Service design!](https://gitlab.com/edwardzhou/tiny_url_assigment/raw/master/assets/service-design.png)



## Approach


According to the requirement, there are several outstanding points:
1. Less write - there is no more change after the tiny url been generated.
2. Random read - it depends on the spread of the tiny url.
3. Unpredictable concurrency - it will be high volumn access if the url is relate to Hot Topics. the other cases will not much.
4. Url lifecycle - the url can be expired/deleted after a specific period.

AWS API Gateway + Lambda + DynamoDB is a quite good approach to satisfy the points above:
1. Easy to implement, test, deploy
> Lambda (FaaS) is quite simple. And CloudFormation can help to allocate the required resources and deploy to.

2. Auto scale
> Lambda is auto scalable

3. Cost save
> Lambda only be billed on usage. There is no payment for the idle. 
> It is even free for low volume access since AWS offers Free Tier (1M free requests per month and 400,000 GB-seconds of compute time per month.)


## Business Outcome


【quoted from Wikipedia [https://en.wikipedia.org/wiki/URL_shortening]】

A friendly URL may be desired for messaging technologies that limit the number of characters in a message (for example SMS), for reducing the amount of typing required if the reader is copying a URL from a print source, for making it easier for a person to remember, or for the intention of a permalink. In November 2009, the shortened links of the URL shortening service Bitly were accessed 2.1 billion times.


## Go Live

This is a preliminary version. The following should be considered before goes live:
1. more flexible and robust security with IAM or Cognito.
2. use usage-plan to limit the budget.
3. use custom domain the replace the default one AWS offered.
4. add TTL to url to to discard the aged url, saving cost from DynamoDB usage.

## Api-Doc

### make new tiny url

```bash
curl -H 'x-app-key <app-key>' -d '{"url": "https://www.163.com/def1"}' https://a0xmhgdh7l.execute-api.us-west-1.amazonaws.com/latest/g/tinyUrl
```

**success:**

http status: 200

```json
{
  "scode": "fdped3dpwl",
  "rawUrl": "https://www.163.com/def1",
  "md5":" f988cb1f41aa74da34c44845a722db3c",
  "tinyUrl": "https://tcooo.co/fdped3dpwl",
  "awsUrl": "https://a0xmhgdh7l.execute-api.us-west-1.amazonaws.com/latest/g/u/fdped3dpwl"
}

tinyUrl is the shorten url with custom domain.
awsUrl is the url with default domain aws offers for testing.
```

**if the url is made before:**

http status: 500
```
https://www.163.com/def1 already exists
```

**if the app-key is invalid:**

http status: 403
```
Authentication required
```

## System implementation

Lambda function is very simple, but kind of trivia if be utilized for restful web development.
every function requires a pare of API Gateway and Function definitions in template.yaml.

So I build an auto restful router make the web development more faster and easier.

### Restful Auto Router

implement restful router and sample.

The restful router can extremely simplify the development with API Gateway + Lambda.

### The restful router supports two approach:
#### 1. wrap the single controller class as Restful style.


```javascript
/**
 * router for restful protocol.
 * HTTP VER   Resources          handler method
 * GET        /resources    ->   controllerClass#list
 * POST       /resources    ->   controllerClass#create
 * GET        /resources/id ->   controllerClass#show
 * PUT        /resources/id ->   controllerClass#update
 * DELETE     /resources/id ->   controllerClass#destroy
 * OPTIONS     /resources    ->   controllerClass#options (optional)
 *
 * controllerClass can only implement supported actions. eg. only #list and #create
 * those unimplemented actions will get HTTP 501.
 *
 * @param {*} controllerClass - controller class to wrap with Restful style
 */

```

Sample controller

```javascript
class TestController {
  /**
   * GET /resources
   * to retrieve the list of resources
   */
  async list(apiGateway, params, callback) {
    const response = {
      statusCode: 200,
      headers: {},
      body: 'Hello AWS Lambda~ with CI/CD'
    }

    return callback(null, response)
  }

  /**
   * POST /resources
   * to create a new resource
   */
  async create(apiGateway, params, callback) {
    const response = {
      statusCode: 200,
      headers: {},
      body: 'new resource created.'
    }

    return callback(null, response)
  }

}


module.exports = {
  clazz: TestController, // for auto router
  TestController
}
```

There is no ugly code like

```javascript

exports.handler = function(event, context, callback) {
  context.callbackWaitsForEmptyEventLoop = false

  if (event.httpMethod == 'GET' && event.pathParameters.proxy == null) {
     // handle GET /resources
     // .....
  } else if (event.httpMethod == 'GET' && event.pathParameters.proxy != null) {
     // handle GET /resources/id
     // .....
  } else if (event.httpMethod == 'POST' && event.pathParameters.proxy == null) {
     // handle POST /resources
     // .....
  } // else if ......

  return callback(null, response)
}

```

#### Pros: Easy to write, Easy to test.
#### Cons: Will double the api configurations in template.yaml


#### 2. Auto Router

With restful router, Each Restful API (controller) has 4 configurations,
2 for API-Gateway
2 for Lambda

this is kind of dummy and boring. It can be resolved with auto router.

Auto Router will dispatch the request '/g/{target}/{proxy} to the correspond controller in conventional.
for instance:
* GET /g/test, will be dispatched to src/controllers/public/testController#list
* GET /g/users, will be dispatched to src/controllers/public/usersController#list

Namespace (subfolder) is also supported:

* GET /g/accounts.profile, will be dispatched to src/controllers/public/accounts/profileController#list


## Test Coverage

![Test Coverage](https://gitlab.com/edwardzhou/tiny_url_assigment/raw/master/assets/test-coverage.png)


### how to run test

```bash
npm i
npm run test-with-coverage
```