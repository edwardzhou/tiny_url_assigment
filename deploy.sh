#!/bin/sh

# variables
STACK_NAME=TinyUrlAssigment
S3_BUCKET=edward-demo-test

# prepare output directory
[ ! -d ./output ] && mkdir ./output

# package
sam package --template-file ./template.yaml \
  --output-template-file ./output/serverless-output.yaml \
  --s3-bucket $S3_BUCKET

# deploy
aws cloudformation deploy \
  --template-file ./output/serverless-output.yaml \
  --stack-name $STACK_NAME \
  --s3-bucket $S3_BUCKET \
  --capabilities CAPABILITY_IAM \
  --parameter-overrides \
  XAppKey=foobar \
  CustomDomain=txx.co


# show deployment
aws cloudformation describe-stacks --stack-name $STACK_NAME
